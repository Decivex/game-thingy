CC=g++
LINKS=-lglfw -lGLEW -lGL
INCLUDES=-Iheaders

SDIR=src
ODIR=obj

OUT=main.out

_OBJS = gl_textures.o camera.o gl_buffers.o gl_debug_utils.o gl_shaders.o \
       	file_utils.o window.o main.o shapes.o
OBJS = $(patsubst %,$(ODIR)/%,$(_OBJS))

all: $(OBJS)
	$(CC) -Wall -pedantic -o $(OUT) $(OBJS) $(LINKS)

.PHONY: run

run: all
	chmod +x $(OUT)
	./$(OUT)

$(ODIR)/%.o: $(SDIR)/%.cpp
	$(CC) $(INCLUDES) -c -o $@ $<

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o
	rm $(OUT)
