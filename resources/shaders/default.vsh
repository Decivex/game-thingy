#version 330 core
attribute vec3 vPos;
attribute vec2 vTex;
attribute vec3 vNormal;

varying vec2 texCoord;
varying float depth;
varying vec3 N;
varying vec4 worldPos;

uniform mat4 MVP;
uniform mat4 M;
uniform mat4 V;

void main() {
  gl_Position = MVP * vec4(vPos, 1.0);
  depth = gl_Position.z;
  texCoord = vTex;

  mat3 NormalMatrix = transpose(inverse(mat3(M)));


  N = NormalMatrix * vNormal;
  worldPos = M * vec4(vPos, 1.0);
}
