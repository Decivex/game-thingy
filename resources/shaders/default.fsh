#version 330 core
varying vec2 texCoord;
varying float depth;
varying vec3 N;
varying vec4 worldPos;

uniform vec3 lightPos;

uniform sampler2D tex;

uniform vec3 directionalLight_direction;
uniform vec3 directionalLight_lightColor;
uniform vec3 directionalLight_shadowColor;


void main() {
  vec4 texColor = texture2D(tex, texCoord);
  vec3 texRGB = texColor.xyz;
  float alpha = texColor.w;

  // Calculate point light intensity
  vec3 lightVector = lightPos - worldPos.xyz;
  float diff = max(dot(N, normalize(lightVector)), 0);
  vec3 pointColor = vec3(diff);

  // Calculate hemilight
  float diral = dot(N, normalize(directionalLight_direction)) * 0.5 + 0.5;
  vec3 hemiColor = mix(directionalLight_shadowColor, directionalLight_lightColor, diral);


  vec3 lightColor = min(hemiColor*.5 + pointColor*.5, vec3(1));

  gl_FragColor = vec4(lightColor * texRGB, alpha);
}
