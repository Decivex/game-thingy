#version 330 core
attribute vec3 vPos;

uniform mat4 VP;

void main() {
	gl_Position = VP * vec4(vPos, 1);
	gl_PointSize = 20.0;
}