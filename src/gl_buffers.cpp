#include "gl_buffers.h"


VBO::VBO() {
    glGenBuffers(1, &this->id);
}

VBO::~VBO() {
    glDeleteBuffers(1, &this->id);
}

void VBO::bind(GLenum target) {
    glBindBuffer(target, this->id);
}

std::shared_ptr<VBO> VBO::init(GLenum target) {
	auto ptr = std::make_shared<VBO>();
	ptr->bind(target);
	return ptr;
}

VAO::VAO() {
    glGenVertexArrays(1, &this->id);
}

VAO::~VAO() {
    glDeleteVertexArrays(1, &this->id);
}

void VAO::bind() {
    glBindVertexArray(this->id);
}

void VAO::unbind() {
    glBindVertexArray(0);
}

void VAO::attribPointer(
    unsigned int index,
    int size,
    GLenum type,
    int stride,
    size_t offset,
    bool normalized
) {
	glEnableVertexArrayAttrib(this->id, index);
	glVertexAttribPointer(
		(GLuint) index,
		(GLint) size,
		type,
		(GLboolean) normalized,
		(GLsizei) stride,
		(void*) offset
	);
}


std::shared_ptr<VAO> VAO::init() {
	auto ptr = std::make_shared<VAO>();
	ptr->bind();
	return ptr;
}