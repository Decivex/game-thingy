#include "gl_textures.h"
#include "file_utils.h"
#include <iostream>

Texture::Texture() {
    glGenTextures(1, &this->id);
}


Texture::~Texture() {
    glDeleteTextures(1, &this->id);
}


void Texture::bind(GLenum target) {
    glBindTexture(target, this->id);
}


std::shared_ptr<Texture> Texture::loadRaw(
    std::string fileName,
    Dimensions size,
    GLint internalFormat,
    GLenum format
) {
    std::vector<unsigned char> data = Util::read_binary_file(fileName);

    auto texture = std::make_shared<Texture>();
    texture->bind(GL_TEXTURE_2D);
    glTexImage2D(
        GL_TEXTURE_2D,
        0, // Detail level
        internalFormat,
        size.width,
        size.height,
        0, // Border
        format,
        GL_UNSIGNED_BYTE,
        data.data()
    );

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    return texture;
}
