#include "file_utils.h"
#include <algorithm>
#include <cerrno>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <iterator>
#include <stdexcept>
#include <vector>

std::string Util::read_file(const std::string& filename) {
    std::FILE* file_ptr = std::fopen(filename.c_str(), "rb");

    if(!file_ptr) {
        throw std::runtime_error(filename + ": " + strerror(errno));
    }

    std::string file_content;
    std::fseek(file_ptr, 0, SEEK_END);
    file_content.resize(std::ftell(file_ptr));
    std::rewind(file_ptr);
    std::fread(&file_content[0], 1, file_content.size(), file_ptr);
    std::fclose(file_ptr);
    return file_content;
}

std::vector<BYTE> Util::read_binary_file(const std::string& filename) {
    std::ifstream file(filename.c_str(), std::ios::binary);
    file.unsetf(std::ios::skipws);

    std::streampos size;
    file.seekg(0, std::ios::end);
    size = file.tellg();
    file.seekg(0, std::ios::beg);

    std::vector<BYTE> file_content;
    file_content.reserve(size);

    file_content.insert(
        file_content.begin(),
        std::istream_iterator<BYTE>(file),
        std::istream_iterator<BYTE>()
    );

    return file_content;
}
