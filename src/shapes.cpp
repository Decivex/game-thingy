#include "shapes.h"
#include <glm/gtc/constants.hpp>
#include <vector>


Mesh::Mesh(int vertex_count, int tri_count, bool has_normals) {
	this->vertex_count = vertex_count;
	this->tri_count = tri_count;
	this->has_normals = has_normals;

	this->vertices = new glm::vec3[vertex_count];
	this->tris = new Triangle[tri_count];

	if(has_normals)
		this->normals = new glm::vec3[vertex_count];
	else
		this->normals = nullptr;
}


Mesh::~Mesh() {
	delete[] this->vertices;
	delete[] this->tris;
	delete[] this->normals;
}


Mesh generate_cylinder(int rad_segments, int vert_segments) {
	// Generate a cylinder with the specified radian and vertical segments.
	// The cylinder contains a seam so that textures coordinates can be used
	// on it.
	rad_segments = std::max(rad_segments, 3);
	vert_segments = std::max(vert_segments, 1);

	int vertex_count = (vert_segments + 1) * (rad_segments + 1);
	int tri_count = rad_segments * vert_segments * 2;

	float y_step = 2.0f / vert_segments;
	float r_step = glm::pi<float>() * 2.0f / vert_segments;

	Mesh cyl(vertex_count, tri_count, true);

	auto coords = [&](int x, int y) -> int {
		return y * (rad_segments + 1) + x;
	};

	// Create vertices
	for(int i=0; i<=vert_segments; i++)
	for(int j=0; j<=rad_segments; j++) {
		float x = glm::sin(r_step * j);
		float y = -1.0f + y_step * i;
		float z = glm::cos(r_step * j);

		cyl.vertices[coords(j, i)] = glm::vec3(x, y, z);
		cyl.normals[coords(j, i)] = glm::vec3(x, 0, z);
	}

	int idx = 0;

	// Create tris
	for(int i=0; i<vert_segments; i++)
	for(int j=0; j<rad_segments; j++) {
		cyl.tris[idx++] = {coords(j, i), coords(j+1, i), coords(j, i+1)};
		cyl.tris[idx++] = {coords(j+1, i), coords(j+1, i+1), coords(j, i+1)};
	}

	return cyl;
}

Mesh generate_cube() {
	// Six faces, two tris per face
	int tri_count = 6 * 2;
	// Four vertices per face
	int vert_count = 6 * 4;

	Mesh cube(vert_count, tri_count, true);


	const int face_data[] = {
		0, +1,
		0, -1,
		1, +1,
		1, -1,
		2, +1,
		2, -1,
	};

	// Axis patterns
	const int a[6][4] = {
		// + faces
		{+1, +1, +1, +1},
		{-1, +1, +1, -1},
		{-1, -1, +1, +1},

		// - faces
		{-1, -1, -1, -1},
		{-1, +1, +1, -1},
		{+1, +1, -1, -1},
	};


	for(int i=0; i<6; i++) {
		int o = i*4; // First vertex idx
		int f = i*2; // First face idx

		int axis = face_data[f];
		int direction = face_data[f+1];

		// Positions
		int offset = (direction==-1)*3;
		for(int j=0; j<4; j++) {
			cube.vertices[o+j] = glm::vec3(
				a[(3-axis)%3+offset][j],
				a[(4-axis)%3+offset][j],
				a[(5-axis)%3+offset][j]
			);
		}

		// Normals
		glm::vec3 normal;
		normal[axis] = direction;

		cube.normals[o+0] = normal;
		cube.normals[o+1] = normal;
		cube.normals[o+2] = normal;
		cube.normals[o+3] = normal;

		// Triangles
		cube.tris[f+0] = {o+0, o+1, o+2};
		cube.tris[f+1] = {o+0, o+2, o+3};
	}

	return cube;
}
