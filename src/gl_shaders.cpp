#include "gl_shaders.h"
#include <iostream>
#include <vector>

#include "gl_debug_utils.h"

GLuint load_shader(std::string filename, GLuint shader_type) {
    std::string shader_code = Util::read_file(filename);
    const char *shader_cstr = shader_code.c_str();

    GLuint shader = glCreateShader(shader_type);
    glShaderSource(shader, 1, &shader_cstr, NULL);
    glCompileShader(shader);

    GLint isCompiled = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
    if(isCompiled == GL_FALSE) {
      GLint maxLength = 0;
      glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

      // The maxLength includes the NULL character
      std::vector<GLchar> errorLog(maxLength);
      glGetShaderInfoLog(shader, maxLength, &maxLength, &errorLog[0]);

      std::cout << &errorLog[0] << std::endl;
    }
    return shader;
}

Shader load_shader_program(std::string base_filename) {
    // TODO: Move into proper utility file and add error checks
    GLuint vertex_shader = load_shader(base_filename+".vsh", GL_VERTEX_SHADER);
    GLuint fragment_shader = load_shader(base_filename+".fsh", GL_FRAGMENT_SHADER);

    GLuint program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);

    Shader shader;
    shader.id = program;
    return shader;
}

void Shader::use() {
    glUseProgram(this->id);
}


GLuint Shader::attrib(const char* name) const {
    return glGetAttribLocation(this->id, name);
}

GLuint Shader::uniform(const char* name) const {
    return glGetUniformLocation(this->id, name);
}
