#include <iostream>
#include <memory>
#include <vector>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include "window.h"
#include "file_utils.h"
#include "gl_shaders.h"
#include "gl_debug_utils.h"
#include "gl_buffers.h"
#include "gl_textures.h"
#include "camera.h"
#include "vertices.h"
#include "shapes.h"


const std::string SHADER_DIRECTORY = "resources/shaders/";
float rotation = 0.0f;
glm::vec3 lightPosition(3.0f, 5.0f, 1.0f);
OrbitCamera camera;


struct DirectionalLight {
    glm::vec3 direction;
    glm::vec3 lightColor;
    glm::vec3 shadowColor;
};


class DirectionalLightUniform {
public:
    GLint direction;
    GLint lightColor;
    GLint shadowColor;

    DirectionalLightUniform(const Shader& shader) {
        this->direction = glGetUniformLocation(shader.id, "directionalLight_direction");
        this->lightColor = glGetUniformLocation(shader.id, "directionalLight_lightColor");
        this->shadowColor = glGetUniformLocation(shader.id, "directionalLight_shadowColor");
    }

    void send(const DirectionalLight& directionalLight) {
        glUniform3fv(this->direction, 1, glm::value_ptr(directionalLight.direction));
        glUniform3fv(this->lightColor, 1, glm::value_ptr(directionalLight.lightColor));
        glUniform3fv(this->shadowColor, 1, glm::value_ptr(directionalLight.shadowColor));
    }
};



DirectionalLight sun = {
    glm::vec3(0.0f, -1.0f, 0.0f),
    glm::vec3(0.20, 0.52, 1.00),
    glm::vec3(1.00, 0.78, 0.50)
};


// Texture unit
const float TXU = 1.0 / 16.0;

std::vector<glm::vec2> cube_texcoords = {
    // +X face
    glm::vec2(TXU, TXU+TXU),
    glm::vec2(TXU, 0.0+TXU),
    glm::vec2(0.0, 0.0+TXU),
    glm::vec2(0.0, TXU+TXU),

    // -X face
    glm::vec2(TXU, TXU+TXU),
    glm::vec2(TXU, 0.0+TXU),
    glm::vec2(0.0, 0.0+TXU),
    glm::vec2(0.0, TXU+TXU),

    // +Y face
    glm::vec2(TXU+TXU, TXU+TXU),
    glm::vec2(TXU+TXU, 0.0+TXU),
    glm::vec2(0.0+TXU, 0.0+TXU),
    glm::vec2(0.0+TXU, TXU+TXU),

    // -Y face
    glm::vec2(0.0+TXU, 0.0),
    glm::vec2(TXU+TXU, 0.0),
    glm::vec2(TXU+TXU, TXU),
    glm::vec2(0.0+TXU, TXU),

    // +Z face
    glm::vec2(0.0, TXU+TXU),
    glm::vec2(TXU, TXU+TXU),
    glm::vec2(TXU, 0.0+TXU),
    glm::vec2(0.0, 0.0+TXU),

    // -Z face
    glm::vec2(TXU, 0.0+TXU),
    glm::vec2(0.0, 0.0+TXU),
    glm::vec2(0.0, TXU+TXU),
    glm::vec2(TXU, TXU+TXU),
};


void scroll_callback(GLFWwindow* window, double xoff, double yoff) {

    int state = glfwGetKey(window, GLFW_KEY_LEFT_SHIFT);
    if (state == GLFW_PRESS)
        lightPosition.y += yoff;
    else {
        double scroll = 1.0f + 0.1f * -yoff;
        camera.distance *= scroll;
    }
}


void cursor_callback(GLFWwindow* window, double x, double y) {
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    glm::vec2 cursor(x, y);
    glm::vec2 center(width * 0.5f, height * 0.5f);

    glm::vec2 delta = cursor - center;

    double theta = delta.x * 0.005f;
    double phi = delta.y * 0.005f;

    int state = glfwGetKey(window, GLFW_KEY_LEFT_SHIFT);
    
    if (state == GLFW_PRESS)
        lightPosition += glm::rotateY(glm::vec3(theta, 0, phi), camera.theta) * 3.0f;
    else
        camera.rotate(-theta, phi);

    glfwSetCursorPos(window, center.x, center.y);
}


void setup_controls(const Window& window) {
  // Setup controls
  glfwSetInputMode(window.window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  glfwSetCursorPosCallback(window.window, cursor_callback);
  glfwSetScrollCallback(window.window, scroll_callback);
}


void debug_setup() {
    Util::Debug::registerGLErrorHandler();
    std::cout << "Renderer: " << glGetString(GL_RENDERER) << std::endl;
}


template <class T>
void send_buffer_data(GLenum target, GLenum usage, std::vector<T> &data) {
    glBufferData(
        target,
        sizeof(T) * data.size(),
        &data[0],
        usage
    );
}


void send_buffer_data(GLenum target, GLenum usage, DataInfo di) {
    glBufferData(
        target,
        di.size,
        di.data,
        usage
    );
}


Model setup_cube(const Shader& program) {
    GLuint vpos_location = program.attrib("vPos");
    GLuint vtex_location = program.attrib("vTex");
    GLuint vnormal_location = program.attrib("vNormal");

    auto vao = VAO::init();

    Mesh cub = generate_cube();
    
    auto vertex_buffer = VBO::init(GL_ARRAY_BUFFER);
    send_buffer_data(GL_ARRAY_BUFFER, GL_STATIC_DRAW, cub.vertex_data());

    vao->attribPointer(
        vpos_location,
        3,
        GL_FLOAT,
        sizeof(glm::vec3),
        0
    );
    
    auto normal_buffer = VBO::init(GL_ARRAY_BUFFER);
    send_buffer_data(GL_ARRAY_BUFFER, GL_STATIC_DRAW, cub.normal_data());
    
    vao->attribPointer(
        vnormal_location,
        3,
        GL_FLOAT,
        sizeof(glm::vec3),
        0
    );
    
    auto texcoords_buffer = VBO::init(GL_ARRAY_BUFFER);
    send_buffer_data(GL_ARRAY_BUFFER, GL_STATIC_DRAW, cube_texcoords);

    vao->attribPointer(
        vtex_location,
        2,
        GL_FLOAT,
        sizeof(cube_texcoords[0]),
        0
    );

    auto elementbuffer = VBO::init(GL_ELEMENT_ARRAY_BUFFER);
    send_buffer_data(GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW, cub.tri_data());

    auto texture = Texture::loadRaw(
        "resources/textures/atlas.data",
        {512, 512}, GL_RGBA, GL_RGBA
    );

    return {
        vao,
        {
            vertex_buffer,
            normal_buffer,
            elementbuffer,
            texcoords_buffer
        },
        {texture}
    };
}


Model setup_cylinder(const Shader& program) {
    GLuint vpos_location = program.attrib("vPos");
    GLuint vtex_location = program.attrib("vTex");
    GLuint vnormal_location = program.attrib("vNormal");

    auto vao = VAO::init();

    Mesh cyl = generate_cylinder(8, 1);

    auto vertex_buffer = VBO::init(GL_ARRAY_BUFFER);
    send_buffer_data(GL_ARRAY_BUFFER, GL_STATIC_DRAW, cyl.vertex_data());
    vao->attribPointer(
        vpos_location,
        3,
        GL_FLOAT,
        sizeof(cyl.vertices[0]),
        0
    );

    auto normal_buffer = VBO::init(GL_ARRAY_BUFFER);
    send_buffer_data(GL_ARRAY_BUFFER, GL_STATIC_DRAW, cyl.normal_data());
    vao->attribPointer(
        vtex_location,
        3,
        GL_FLOAT,
        sizeof(cyl.normals[0]),
        0
    );

    auto element_buffer = VBO::init(GL_ELEMENT_ARRAY_BUFFER);
    send_buffer_data(GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW, cyl.tri_data());

    auto texture = Texture::loadRaw(
        "resources/textures/atlas.data",
        {512, 512}, GL_RGBA, GL_RGBA
    );

    return {
        vao,
        {vertex_buffer, normal_buffer, element_buffer},
        {texture}
    };
}


int main(int argc, char const *argv[]) {
    Window::initGLFW();

    Window window({4, 3});
    window.open();

    debug_setup();

    Shader program = load_shader_program(SHADER_DIRECTORY+"default");
    Shader pointShader = load_shader_program(SHADER_DIRECTORY+"point");

    setup_controls(window);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glEnable(GL_CULL_FACE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    Model cube = setup_cube(program);
    // Model cylinder = setup_cylinder(program);
    auto pointVAO = std::make_shared<VAO>();

    program.use();
    GLuint gl_M = program.uniform("M");
    GLuint gl_V = program.uniform("V");
    GLuint gl_MVP = program.uniform("MVP");
    GLuint gl_lightPos = program.uniform("lightPos");

    DirectionalLightUniform directionalLight(program);
    std::cout << directionalLight.direction << std::endl;

    pointShader.use();
    GLuint gl_PointPOS = pointShader.attrib("vPos");
    GLuint gl_PointVP = pointShader.uniform("VP");


    window.key_handlers[GLFW_KEY_ESCAPE] = [](Window* win, int action)
    {
        if(action==GLFW_PRESS) win->running = false;
    };


    window.renderfunc = [&]()
    {
        program.use();
        directionalLight.send(sun);

        // Projection matrix
        glm::mat4 projection =  glm::perspective(
            glm::radians(45.0f),
            window.size.aspect_ratio(),
            0.1f,
            1000.0f
        );

        // View matrix
        glm::mat4 view = camera.get_matrix();

        // Model matrix
        glm::mat4 model = glm::translate(glm::vec3(-1.2, 0.0, 0.0));

        // Modelview matrix
        glm::mat4 modelview = view * model;
        glm::mat4 mvp = projection * modelview;

        glUniformMatrix4fv(gl_M, 1, GL_FALSE, glm::value_ptr(model));
        glUniformMatrix4fv(gl_V, 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(gl_MVP, 1, GL_FALSE, glm::value_ptr(mvp));
        glUniform3fv(gl_lightPos, 1, glm::value_ptr(lightPosition));

        cube.vao->bind();
        cube.textures[0]->bind(GL_TEXTURE_2D);
        glDrawElements(GL_TRIANGLES, 6*2*3, GL_UNSIGNED_INT, (void*)0);

        // Model matrix
        rotation += 0.01f;
        model = glm::mat4();
        model = glm::rotate(rotation, glm::vec3(1, 0, 0));
        model = glm::translate(model, glm::vec3(+1.2, 0.0, 0.0));
        // Modelview matrix
        modelview = view * model;
        mvp = projection * modelview;

        glUniformMatrix4fv(gl_M, 1, GL_FALSE, glm::value_ptr(model));
        glUniformMatrix4fv(gl_V, 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(gl_MVP, 1, GL_FALSE, glm::value_ptr(mvp));

        glDrawElements(GL_TRIANGLES, 6*2*3, GL_UNSIGNED_INT, (void*)0);

        // cylinder.vao->bind();
        // cylinder.textures[0]->bind(GL_TEXTURE_2D);
        // glDrawElements(GL_TRIANGLES, 6*2*3, GL_UNSIGNED_INT, (void*)0);

        VAO::unbind();
        pointShader.use();
        pointVAO->bind();
        glm::mat4 vp = projection * view;
        glUniformMatrix4fv(gl_PointVP, 1, GL_FALSE, glm::value_ptr(vp));
        glPointSize(16.0f);
        glVertexAttrib3fv(gl_PointPOS, glm::value_ptr(lightPosition));
        glDrawArrays(GL_POINTS, 0, 1);

    };

    window.run();

    return 0;
}
