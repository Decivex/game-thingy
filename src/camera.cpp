#include "camera.h"
#include <glm/gtx/transform.hpp>

OrbitCamera::OrbitCamera() {
    this->distance = 10.0f;
}

OrbitCamera::OrbitCamera(
    glm::vec3 target,
    float theta,
    float phi,
    float distance
) {
    this->target = target;
    this->theta = theta;
    this->phi = phi;
    this->distance = distance;
}

glm::mat4 OrbitCamera::get_matrix() {
  glm::vec3 camera_offset(
      glm::cos(phi) * glm::sin(theta) * distance,
      glm::sin(phi) * distance,
      glm::cos(phi) * glm::cos(theta) * distance
  );

  glm::vec3 up_vector(
      glm::cos(phi+glm::half_pi<float>()) * glm::sin(theta),
      glm::sin(phi+glm::half_pi<float>()),
      glm::cos(phi+glm::half_pi<float>()) * glm::cos(theta)
  );

  glm::mat4 matrix = glm::lookAt(
      target + camera_offset, target, up_vector
  );

  return matrix;
}

void OrbitCamera::rotate(float theta, float phi) {
    this->theta += theta;
    this->phi += phi;

    if(this->phi > glm::half_pi<float>()) {
        this->phi = glm::half_pi<float>();
    }

    if(this->phi < -glm::half_pi<float>()) {
        this->phi = -glm::half_pi<float>();
    }
}
