#include "window.h"

#include <stdexcept>
#include <sstream>

const int WINDOW_START_WIDTH = 1024;
const int WINDOW_START_HEIGHT = 768;


std::map<GLFWwindow*, Window*> Window::instances;


void error_callback(int error, const char* description) {
    fprintf(stderr, "Error: %s\n", description);
}


void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    auto wit = Window::instances.find(window);

    if(wit==Window::instances.end()) {
        return;
    }

    auto hit = wit->second->key_handlers.find(key);

    if(hit==wit->second->key_handlers.end()) {
        return;
    }

    hit->second(wit->second, action);
}


Window::Window(Version glVersion) {
    this->window = nullptr;
    this->gl_major = glVersion.major;
    this->gl_minor = glVersion.minor;
    this->size = {WINDOW_START_WIDTH, WINDOW_START_HEIGHT};
}


Window::~Window() {
    if(this->window) {
        // Window::instances.erase(this->window);
        glfwDestroyWindow(this->window);
    }
}


void Window::open() {
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, this->gl_major);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, this->gl_minor);
    this->window = glfwCreateWindow(
        WINDOW_START_WIDTH,
        WINDOW_START_HEIGHT,
        "Window",
        NULL,
        NULL
    );
    if(this->window == nullptr) {
        throw std::runtime_error("Failed to open window.");
    }
    Window::instances[this->window] = this;
    glfwSetKeyCallback(this->window, key_callback);
    glfwMakeContextCurrent(this->window);

    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
        std::stringstream ss;
        ss << "Failed to initialize glew! Error: " << glewGetErrorString(err);
        throw std::runtime_error(ss.str());
    }
}


bool Window::shouldClose() {
    return glfwWindowShouldClose(this->window);
}

// static function
void Window::initGLFW() {
    if (!glfwInit()) {
        throw std::runtime_error("Failed to initialize GLFW");
    }

    glfwSetErrorCallback(error_callback);
}

void Window::render() {
    glfwGetFramebufferSize(
        this->window,
        &this->size.width,
        &this->size.height
    );
    glViewport(0, 0, this->size.width, this->size.height);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    this->renderfunc();
    glfwSwapBuffers(this->window);
    glfwPollEvents();
}

void Window::run() {
    while (!this->shouldClose() && this->running) {
        this->render();
    }
}
