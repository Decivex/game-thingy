#pragma once

struct Dimensions {
    int width;
    int height;

    float aspect_ratio() {
    	return (float)this->width / this->height;
    }
};
