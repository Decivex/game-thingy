#pragma once
#include <glm/glm.hpp>

// A camera that is centered on a location
class OrbitCamera {
public:
    OrbitCamera();
    OrbitCamera(glm::vec3 target, float theta, float phi, float distance);
    glm::mat4 get_matrix();
    void rotate(float theta, float phi);
    void move(glm::vec3 offset);
    glm::vec3 target;
    float theta, phi, distance;
};
