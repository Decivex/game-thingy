#pragma once
#include <GL/glew.h>
#include <vector>
#include <memory>
#include "gl_textures.h"


class VBO;

// RAII based VBO class
class VBO {
public:
    VBO();
    ~VBO();
    void bind(GLenum target);
    GLuint id;
    static std::shared_ptr<VBO> init(GLenum target);
};


class VAO;

// RAII based VAO class
class VAO {
public:
    VAO();
    ~VAO();
    GLuint id;
    void bind();
    static void unbind();
    // Wrapper for glVertexAttribPointer and
    // glEnableVertexArrayAttrib. The normalized
    // argument was moved to the end because I expect
    // to use that one the least.
    void attribPointer(
        unsigned int index,
        int size,
        GLenum type,
        int stride = 0,
        size_t offset = 0,
        bool normalized = false
    );
    static std::shared_ptr<VAO> init();
};


struct Model {
    std::shared_ptr<VAO> vao;
    std::vector<std::shared_ptr<VBO>> buffers;
    std::vector<std::shared_ptr<Texture>> textures;
};
