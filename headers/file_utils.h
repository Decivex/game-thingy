#pragma once
#include <string>
#include <vector>

typedef unsigned char BYTE;

namespace Util {
    std::string read_file(const std::string& filename);
    std::vector<BYTE> read_binary_file(const std::string& filename);
}
