#pragma once
#include <GL/glew.h>

namespace Util {
    namespace Debug {
        void messageCallback(
            GLenum source,
            GLenum type,
            GLuint id,
            GLenum severity,
            GLsizei length,
            const GLchar* message,
            const void* userParam
        );

        void registerGLErrorHandler();
    }
}
