#pragma once
#include <glm/glm.hpp>


struct ColoredVertex {
    glm::vec3 position;
    glm::vec4 color;
};


struct ColoredTexturedVertex {
    glm::vec3 position;
    glm::vec2 texCoords;
    glm::vec4 color;
};


struct NormalTexturedVertex {
    glm::vec3 position;
    glm::vec2 texCoords;
    glm::vec3 normal;
};

struct NormalVertex {
	glm::vec3 position;
	glm::vec3 normal;
};
