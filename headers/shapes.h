#pragma once
#include <glm/glm.hpp>


struct DataInfo {
	void* data;
	size_t size;
};


struct Triangle {
	int a, b, c;
};


class Mesh {
public:
	int vertex_count;
	glm::vec3 *vertices;
	DataInfo vertex_data() const {
		return {
			this->vertices,
			sizeof(glm::vec3) * this->vertex_count
		};
	};

	int tri_count;
	Triangle *tris;
	DataInfo tri_data() const {
		return {
			this->tris,
			sizeof(Triangle) * this->tri_count
		};
	};

	bool has_normals;
	glm::vec3 *normals;
	DataInfo normal_data() const {
		return {
			this->normals,
			sizeof(glm::vec3) * this->vertex_count
		};
	};

	Mesh(int vertex_count, int tri_count, bool has_normals=false);
	~Mesh();
};

Mesh generate_cylinder(int rad_segments, int vert_segments);
Mesh generate_cube();
