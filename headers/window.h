#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>
#include <functional>
#include <map>


#include "geometry.h"


struct Version {
    int major, minor;
};


class Window;


typedef std::function<void(Window*, int)> KeyHandler;


class Window {
public:
    Window(Version glVersion);
    ~Window();
    void open();
    bool shouldClose();
    static void initGLFW();
    GLFWwindow* window;
    int gl_major, gl_minor;
    bool running = true;
    Dimensions size;

    std::function<void()> renderfunc;
    void render();
    void run();

    std::map<int, KeyHandler> key_handlers;
    static std::map<GLFWwindow*, Window*> instances;
};
