#pragma once
#include <GL/glew.h>

#include "file_utils.h"

class Shader {
public:
	GLuint id;
	void use();
	GLuint attrib(const char* name) const;
	GLuint uniform(const char* name) const;
};

Shader load_shader_program(std::string base_filename);
GLuint load_shader(std::string filename, GLuint shader_type);
