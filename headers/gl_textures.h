#pragma once
#include <GL/glew.h>
#include <string>
#include <memory>

#include "geometry.h"


class Texture {
public:
    Texture();
    ~Texture();
    void bind(GLenum target);
    GLuint id;
    static std::shared_ptr<Texture> loadRaw(
        std::string fileName,
        Dimensions size,
        GLint internalFormat,
        GLenum format
    );
};
